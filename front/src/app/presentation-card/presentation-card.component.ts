import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from '../core/services/authentication.service';

@Component({
  selector: 'app-presentation-card',
  templateUrl: './presentation-card.component.html',
  styleUrls: ['./presentation-card.component.scss']
})
export class PresentationCardComponent implements OnChanges {
  @Input() presentation: Presentation;
  @Output() rate = new EventEmitter<boolean>();
  @Output() delete = new EventEmitter<string>();
  @Output() edit = new EventEmitter<string>();
  isOwned = false;
  up = 0;
  down = 0;
  upActive = false;
  downActive = false;

  constructor(private authService: AuthenticationService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.presentation) {
      if (this.presentation.creator && this.presentation.creator._id === this.authService.currentUserValue._id) {
        this.isOwned = true;
      }
      this.calcRating(this.presentation.ratings);
    }
  }

  calcRating(ratings: any[]) {
    this.up = 0;
    this.down = 0;
    const myRating = ratings.find(r => r.user === this.authService.currentUserValue._id);
    if (myRating) {
      if (myRating.positive) {
        this.upActive = true;
        this.downActive = false;
      } else {
        this.upActive = false;
        this.downActive = true;
      }
    } else {
      this.upActive = false;
      this.downActive = false;
    }
    ratings.forEach(r => {
      if (r.positive) {
        this.up += 1;
      } else {
        this.down += 1;
      }
    });
  }
}
