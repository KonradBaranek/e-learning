import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { take } from 'rxjs/operators';
import { PresentationService } from '../core/services/presentation.service';
import { AuthenticationService } from '../core/services/authentication.service';
import { ExerciseService } from '../core/services/exercise.service';

@Component({
  selector: 'app-presentation-list',
  templateUrl: './presentation-list.component.html',
  styleUrls: ['./presentation-list.component.scss']
})
export class PresentationListComponent implements OnInit {
  @Input() presentations: Presentation[];
  @Input() exercises: Exercise[];
  @Output() deletePresentation = new EventEmitter<string>();
  @Output() editPresentation = new EventEmitter<string>();
  @Output() ratePresentation = new EventEmitter<string>();
  @Output() deleteExercise = new EventEmitter<string>();
  @Output() editExercise = new EventEmitter<string>();
  @Output() rateExercise = new EventEmitter<string>();
  tabIndex: number;
  private tabNames = ['presentations', 'exercises'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.queryParams.pipe(take(1))
    .subscribe((queryParams) => {
      this.changeTab(queryParams['tab']);
    });
  }

  changeTab(tab: 'presentations' | 'exercises' | number) {
    if (typeof tab === 'string') {
      this.tabIndex = this.tabNames.indexOf(tab);
      if (this.tabIndex < 0) {
        this.tabIndex = 0;
      }
    } else {
      this.tabIndex = tab;
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { tab: this.tabNames[this.tabIndex] },
    });
  }
}
