import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { AuthGuard } from './core/guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { SectionComponent } from './section/section.component';
import { PresentationEditorComponent } from './presentation-editor/presentation-editor.component';
import { ProfileComponent } from './profile/profile.component';
import { ExerciseViewComponent } from './exercise-view/exercise-view.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ShopComponent } from './shop/shop.component';
import { MyPresentationsComponent } from './my-presentations/my-presentations.component';
import { WorksheetsComponent } from './worksheets/worksheets.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { GroupsComponent } from './groups/groups.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { ExerciseEditorComponent } from './exercise-editor/exercise-editor.component';
import { AvatarPageComponent } from './avatar-page/avatar-page.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  { path: '', component: WelcomePageComponent, },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'exercise-editor', component: ExerciseEditorComponent, canActivate: [AuthGuard] },
  { path: 'exercise-editor/:exerciseId', component: ExerciseEditorComponent, canActivate: [AuthGuard] },
  { path: 'presentation-editor', component: PresentationEditorComponent, canActivate: [AuthGuard] },
  { path: 'presentation-editor/:presentationId', component: PresentationEditorComponent, canActivate: [AuthGuard] },
  { path: 'my-presentations', component: MyPresentationsComponent, canActivate: [AuthGuard] },
  { path: 'grade/:grade/section/:section', component: SectionComponent, canActivate: [AuthGuard] },
  { path: 'presentation/:presentation', component: PresentationComponent, canActivate: [AuthGuard] },
  { path: 'exercise/:exercise', component: ExerciseViewComponent, canActivate: [AuthGuard] },
  { path: 'shop', component: ShopComponent, canActivate: [AuthGuard] },
  { path: 'worksheet', component: WorksheetsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'cookie-policy', component: CookiePolicyComponent },
  { path: 'groups', component: GroupsComponent },
  { path: 'terms-of-use', component: TermsOfUseComponent },
  { path: 'help', component: HelpComponent },
  { path: 'avatar-page', component: AvatarPageComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
