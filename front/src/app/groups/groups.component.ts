import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  groupsArray = [
    {
      name: "Żółwie",
      users: 20,
      level: "Łatwy"
    },
    {
      name: "Robaczki",
      users: 1,
      level: "Trudny"
    },
    {
      name: "Lwy",
      users: 123,
      level: "Średni"
    },
  ]

  exampleArray = [
    {
      name: "Banany",
      users: 20,
      level: "Łatwy"
    },
    {
      name: "Bananowe Ludki",
      users: 1,
      level: "Trudny"
    },
    {
      name: "Bananki",
      users: 123,
      level: "Średni"
    },
  ]

  sufix = (users) => {
    if(users === 1) {
      return "osoba"
    } else if (users > 1 && users < 10) {
      return "osoby"
    }
    return "osób"
  }

  onSearch = () => {

  }
}
