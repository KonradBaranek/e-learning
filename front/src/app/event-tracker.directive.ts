import { Directive,HostListener,Input } from '@angular/core';

declare var ga: any;

@Directive({
  selector: '[appEventTracker]'
})

export class EventTrackerDirective {

  @Input('appEventTracker') option:any;

  @HostListener('click', ['$event']) onClick($event){

    (<any>window).ga('send', 'event', this.option.category, this.option.action, {
      hitCallback: function() {

        console.log('Tracking is successful');
      }

    });

  }
  constructor() { }

}
