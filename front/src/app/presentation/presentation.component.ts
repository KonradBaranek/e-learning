import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PresentationService } from '../core/services/presentation.service';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit {
  currentSlideIndex = 0;
  section: Section;
  presentation: Presentation;

  get slides(): Partial<Slide>[] {
    return this.presentation.slides;
  }

  get currentSlide(): Partial<Slide> {
    return this.presentation.slides[this.currentSlideIndex];
  }

  constructor(private route: ActivatedRoute, private presentationService: PresentationService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.presentationService.get(params['presentation']).subscribe(presentation => {
        this.presentation = presentation;
        this.presentationService.progress(presentation._id, 0).subscribe();
      });
    });
  }

  changeSlide(direction: number): void {
    if (direction < 0 && this.currentSlideIndex > 0) {
      this.currentSlideIndex -= 1;
      return;
    }
    if (this.currentSlideIndex < this.slides.length - 1) {
      this.currentSlideIndex += 1;
      this.presentationService.progress(this.presentation._id, this.currentSlideIndex).subscribe();
    }
  }
}
