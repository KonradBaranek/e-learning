import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent {
  @Input() exercise: Exercise;
  @Output() chooseAnswer = new EventEmitter<string>();

  get answers(): Partial<any>[] {
    return this.exercise.answers;
  }
}
