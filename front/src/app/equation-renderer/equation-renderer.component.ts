import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

interface Part {
  eq: boolean;
  text: string;
}

@Component({
  selector: 'app-equation-renderer',
  templateUrl: './equation-renderer.component.html',
})
export class EquationRenderComponent implements OnChanges {
  @Input() text: string;
  parts: Part[];

  ngOnChanges(changes: SimpleChanges) {
    if (changes.text) {
      this.parts = this.processText(changes.text.currentValue);
    }
  }

  processText(text: string): Part[] {
    const eqs = [];
    const newText = text.replace(/\\\[(.*)\\\]/g, (...args) => {
      eqs.push(args[1]);
      return ' $$$ ';
    });
    return newText.split(' ').map(part => {
      if (part === '$$$') {
        return {
          eq: true,
          text: eqs.shift()
        };
      }
      return {
        eq: false,
        text: part
      };
    });
  }
}
