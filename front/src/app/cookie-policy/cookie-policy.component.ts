import { Component, OnInit } from '@angular/core';
import Cookie from 'js-cookie'

@Component({
  selector: 'app-cookie-policy',
  templateUrl: './cookie-policy.component.html',
  styleUrls: ['./cookie-policy.component.scss']
})
export class CookiePolicyComponent implements OnInit {
  functionalActive = false
  targetingActive = false

  constructor() { }

  ngOnInit() {
    if (Cookie.get('FunctionalCookieName') !== undefined) {
      this.functionalActive = true
    } else {
      this.functionalActive = false
    }
    if (Cookie.get('TargetingCookieName') !== undefined) {
      this.targetingActive = true
    } else {
      this.targetingActive = false
    }
  }

  acceptFunctional() {
    this.functionalActive = true
    Cookie.set('FunctionalCookieName','true')
  }

  cancelFunctional() {
    this.functionalActive = false
    Cookie.remove('FunctionalCookieName')
  }

  acceptTargeting() {
    this.targetingActive = true
    Cookie.set('TargetingCookieName','true')
  }

  cancelTargeting() {
    this.targetingActive = false
    Cookie.remove('TargetingCookieName')
  }
}
