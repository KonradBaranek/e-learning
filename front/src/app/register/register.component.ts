import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../core/services/authentication.service';
import {first} from 'rxjs/operators';
import {ErrorStateMatcher} from '@angular/material';

declare var ga: any;

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})


export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.authenticationService.logout();
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      acceptRules: [false, Validators.requiredTrue]
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      });

    this.returnUrl = '/login';
  }

  get f() {
    return this.registerForm.controls;
  }

  get rulesCheckbox() {
    return this.registerForm.get('acceptRules');
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.register(
      this.f.email.value,
      this.f.username.value,
      this.f.password.value
      )
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
          (<any>window).ga('send', 'event', 'Registration', 'Success');
        },
        error => {
          this.error = error;
          this.loading = false;
          (<any>window).ga('send', 'event', 'Registration', 'Failed: '+this.error);
        });
  }

}
