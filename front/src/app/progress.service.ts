import { Injectable } from '@angular/core';
import { Grade, Section, Lesson, UserProgress, Presentation } from './core/models/grade';
import { AuthenticationService } from './core/services/authentication.service';
import { emptyGrade, avatar } from '../mock';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {
  storage: UserProgress[];
  myStorage: UserProgress;
  user: User;

  constructor(private auth: AuthenticationService) {
    this.storage = JSON.parse(localStorage.getItem('users')) as UserProgress[];
    auth.currentUser.subscribe(user => {
      if (!user) {
        return;
      }
      this.user = user;
      this.myStorage = this.storage.find(s => s.email === this.user.email);
    });
  }

  createPresentation(id: string, presentation: Presentation) {
    if (!this.myStorage.presentations) {
      this.myStorage.presentations = [];
    }
    const exists = this.myStorage.presentations.findIndex(pres => pres.id === id);
    if (exists !== -1) {
      this.myStorage.presentations.splice(exists, 1);
    }
    this.myStorage.presentations.push(presentation);
    this.saveStorage();
  }

  saveStorage() {
    localStorage.setItem('users', JSON.stringify(this.storage));
  }

  createUser(user: User) {
    const newUser: UserProgress = {
      ...user,
      grades: [JSON.parse(JSON.stringify(emptyGrade))],
      avatar: avatar
    };
    this.storage.push(newUser);
    this.saveStorage();
  }

  getPresentation(id: string): Presentation {
    return this.myStorage.presentations.find(pres => pres.id === id);
  }

  getGrades(): Grade[] {
    return this.myStorage.grades;
  }

  getGrade(gradeId: string): Grade {
    return this.myStorage.grades.find(grade => grade.id === gradeId);
  }

  getSection(gradeId: string, sectionId: string): Section {
    return this.myStorage.grades.find(grade => grade.id === gradeId).sections.find(section => section.id === sectionId);
  }

  updateGradeProgress(grade: Grade, value: number) {
    grade.progress.value = value;
    this.saveStorage();
  }

  updateSectionProgress(grade: Grade, section: Section, value: number) {
    section.progress.value = value;
    if (value >= section.progress.maxValue) {
      this.updateGradeProgress(grade, grade.progress.value + 1);
    }
  }

  updateLessonProgress(grade: Grade, section: Section, lesson: Lesson, value: number) {
    lesson.progress.value = value;
    if (value >= lesson.progress.maxValue) {
      this.updateSectionProgress(grade, section, section.progress.value + 1);
    }
  }

  updatePresentationProgress(grade: Grade, section: Section, lesson: Lesson, value: number) {
    // const grade = this.storage.grades.find(g => g.id === gradeId);
    // const section = grade.sections.find(s => s.id === sectionId);
    // const lesson = section.lessons.find(l => l.id === lessonId);
    lesson.presentation.progress.value = value;
    if (value >= lesson.presentation.progress.maxValue) {
      this.updateLessonProgress(grade, section, lesson, lesson.progress.value + 1);
    }
  }

  getAvatar() {
    return this.myStorage.avatar;
  }

  updateAvatar(newAvatar: any) {
    this.myStorage.avatar = newAvatar;
    this.saveStorage();
  }
}
