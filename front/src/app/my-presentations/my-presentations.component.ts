import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { take } from 'rxjs/operators';
import { PresentationService } from '../core/services/presentation.service';
import { AuthenticationService } from '../core/services/authentication.service';
import { ExerciseService } from '../core/services/exercise.service';

@Component({
  selector: 'app-my-presentations',
  templateUrl: './my-presentations.component.html',
  styleUrls: ['./my-presentations.component.scss']
})
export class MyPresentationsComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private presentationService: PresentationService,
    private exerciseService: ExerciseService,
    private authService: AuthenticationService
  ) {}
  presentations: Presentation[];
  exercises: Exercise[];
  grade: Grade;
  section: Section;
  tabIndex: number;
  private tabNames = ['presentations', 'exercises'];

  ngOnInit() {
    this.route.queryParams.pipe(take(1))
    .subscribe((queryParams) => {
      this.presentationService.listByCreator('name', 'asc', this.authService.currentUserValue._id)
      .subscribe(presentations => this.presentations = presentations);
      this.exerciseService.listByCreator('name', 'asc', this.authService.currentUserValue._id)
      .subscribe(exercises => this.exercises = exercises);
      this.changeTab(queryParams['tab']);
    });
  }

  changeTab(tab: 'presentations' | 'exercises' | number) {
    if (typeof tab === 'string') {
      this.tabIndex = this.tabNames.indexOf(tab);
      if (this.tabIndex < 0) {
        this.tabIndex = 0;
      }
    } else {
      this.tabIndex = tab;
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { tab: this.tabNames[this.tabIndex] },
    });
  }

  deletePresentation(id: string) {
    this.presentationService.delete(id).subscribe(pres => {
      this.presentations = this.presentations.filter(p => p._id !== pres._id);
    });
  }

  editPresentation(id: string) {
    this.router.navigate(['/presentation-editor', id]);
  }

  deleteExercise(id: string) {
    this.exerciseService.delete(id).subscribe(pres => {
      this.exercises = this.exercises.filter(p => p._id !== pres._id);
    });
  }

  editExercise(id: string) {
    this.router.navigate(['/exercise-editor', id]);
  }
}
