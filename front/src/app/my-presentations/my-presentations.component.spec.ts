import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPresentationsComponent } from './my-presentations.component';

describe('MyPresentationsComponent', () => {
  let component: MyPresentationsComponent;
  let fixture: ComponentFixture<MyPresentationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPresentationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPresentationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
