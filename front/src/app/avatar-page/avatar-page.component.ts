import { Component, OnInit } from '@angular/core';
import { AvatarService } from '../core/services/avatar.service';
import { AuthenticationService } from '../core/services/authentication.service';

@Component({
  selector: 'app-avatar-page',
  templateUrl: './avatar-page.component.html',
  styleUrls: ['./avatar-page.component.scss']
})
export class AvatarPageComponent implements OnInit {
  loading = false;
  avatar: Avatar;
  svg: String;
  foodAnimate = false;
  loveAnimate = false;

  constructor(private avatarService: AvatarService, private authService: AuthenticationService) {

  }

  animateFood() {
    this.foodAnimate = true;
    setTimeout(() => {
      this.foodAnimate = false;
    }, 5000);
  }

  animateLove() {
    this.loveAnimate = true;
    setTimeout(() => {
      this.loveAnimate = false;
    }, 5000);
  }

  ngOnInit() {
    this.avatarService.get().subscribe(data => {
      this.avatar = data;
    });
  }

}
