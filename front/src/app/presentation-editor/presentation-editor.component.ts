import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PresentationService } from '../core/services/presentation.service';
import { SectionService } from '../core/services/section.service';

declare var ga: any;

@Component({
  selector: 'app-presentation-editor',
  templateUrl: './presentation-editor.component.html',
  styleUrls: ['./presentation-editor.component.scss']
})
export class PresentationEditorComponent implements OnInit {
  presentation: Presentation;
  currentSlideIndex = 0;
  grades: Grade[];
  section: string;
  slides: Partial<Slide>[] = [this.generateNewSlide(this.currentSlideIndex + 1)];
  get current() {
    return this.slides[this.currentSlideIndex];
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private presentationService: PresentationService,
    private sectionService: SectionService
  ) {}

  ngOnInit() {
    this.sectionService.listGrades().subscribe(grades => {
      this.grades = grades;
      this.section = grades[0].sections[0].name;
    });
    this.route.params.subscribe(params => {
      const presid = params['presentationId'];
      if (presid) {
        this.presentationService.get(presid).subscribe(presentation => {
          this.presentation = presentation;
          this.slides = this.presentation.slides;
        });
      }
    });
  }

  private generateNewSlide(number: number) {
    const slide = {
      title: 'Slajd ' + number,
      description: 'Opis slajdu ' + number
    };
    return slide;
  }

  deleteSlide() {
    if (this.slides.length <= 1) {
      return;
    }
    this.slides.splice(this.currentSlideIndex, 1);
    this.currentSlideIndex -= 1;
  }

  changeSlide(direction: number): void {
    if (direction < 0 && this.currentSlideIndex > 0) {
      this.currentSlideIndex -= 1;
      return;
    }
    this.currentSlideIndex += 1;
    if (this.currentSlideIndex >= this.slides.length) {
      this.slides.push(this.generateNewSlide(this.currentSlideIndex + 1));
    }
  }

  savePresentation() {
    const pres = {
      name: this.slides[0].title,
      section: this.section,
      slides: this.slides
    };
    if (this.presentation) {
      this.presentationService.edit(this.presentation._id, pres).subscribe(presentation => {
        this.router.navigateByUrl(`/presentation-editor/${presentation._id}`);
      });
      (<any>window).ga('send', 'event', "PresentationEditor", "Edit-"+this.section);
    } else {
      this.presentationService.create(pres).subscribe(presentation => {
        this.router.navigateByUrl(`/presentation-editor/${presentation._id}`);
      });
      (<any>window).ga('send', 'event', "PresentationEditor", "Create-"+this.section);
    }
  }

  createPresentation() {
    const pres = {
      name: this.slides[0].title,
      section: this.section,
      slides: this.slides
    };
    if (this.presentation) {
      this.presentationService.edit(this.presentation._id, pres).subscribe(presentation => {
        this.router.navigateByUrl(`/my-presentations`);
      });
      (<any>window).ga('send', 'event', "PresentationEditor", "Edit&Send-"+this.section);
    } else {
      this.presentationService.create(pres).subscribe(presentation => {
        this.router.navigateByUrl(`/my-presentations`);
      });
      (<any>window).ga('send', 'event', "PresentationEditor", "Create&Send-"+this.section);
    }
  }
}
