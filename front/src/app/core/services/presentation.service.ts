import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map, pluck} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class PresentationService {
  private baseUrl = `${environment.apiUrl}/presentation/`;
  constructor(private http: HttpClient) {}

  get(id: string): Observable<Presentation> {
    return this.http.get<any>(this.baseUrl + id).pipe(
      pluck('data')
    );
  }

  list(sort: string, sortDir: 'asc' | 'desc'): Observable<Presentation[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir
      }
    }).pipe(
      pluck('data')
    );
  }

  listBySection(sort: string, sortDir: 'asc' | 'desc', section: string): Observable<Presentation[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir, section
      }
    }).pipe(
      pluck('data')
    );
  }

  listByCreator(sort: string, sortDir: 'asc' | 'desc', creator: string): Observable<Presentation[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir, creator
      }
    }).pipe(
      pluck('data')
    );
  }

  create(presentation: Partial<Presentation>): Observable<Presentation> {
    return this.http.post<any>(this.baseUrl, presentation).pipe(
      pluck('data')
    );
  }

  rate(id: string, positive: boolean): Observable<Presentation> {
    return this.http.patch<any>(this.baseUrl + 'rate/' + id , { positive }).pipe(
      pluck('data')
    );
  }

  edit(id: string, presentation: Partial<Presentation>): Observable<Presentation> {
    return this.http.patch<any>(this.baseUrl + 'edit/' + id , {
      name: presentation.name,
      section: presentation.section,
      slides: presentation.slides
    }).pipe(
      pluck('data')
    );
  }

  delete(id: string): Observable<Presentation> {
    return this.http.delete<any>(this.baseUrl + id).pipe(
      pluck('data')
    );
  }

  progress(id: string, page: number): Observable<Presentation> {
    return this.http.patch<any>(this.baseUrl + 'progress/' + id , { page }).pipe(
      pluck('data')
    );
  }
}
