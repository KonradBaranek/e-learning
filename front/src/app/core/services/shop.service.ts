import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map, pluck} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class ShopService {
  private baseUrl = `${environment.apiUrl}/shop/`;
  constructor(private http: HttpClient) {}

  get(id: string): Observable<Outfit> {
    return this.http.get<any>(this.baseUrl + id).pipe(
      pluck('data')
    );
  }

  list(): Observable<Outfit[]> {
    return this.http.get<any>(this.baseUrl).pipe(
      pluck('data')
    );
  }

  buy(id: string): Observable<any> {
    return this.http.patch<any>(this.baseUrl + id, {}).pipe(
      pluck('data')
    );
  }
}
