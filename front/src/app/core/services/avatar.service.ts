import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class AvatarService {
  private baseUrl = `${environment.apiUrl}/avatar/`;
  constructor(private http: HttpClient) { }

  get(): Observable<Avatar> {
    return this.http.get<any>(this.baseUrl).pipe(
      pluck('data'), map((avatar: Avatar) => {
        avatar.svg = this.getAvatarSVG(avatar);
        return avatar;
      })
    );
  }

  wear(id: string): Observable<Outfit[]> {
    return this.http.patch<any>(this.baseUrl + id, {}).pipe(
      pluck('data')
    );
  }


  getAvatarSVG(avatar: Avatar): string {
    let svg = avatar.svg;
    if (this.getHat(avatar)) {
      svg = svg.replace('<hat>', this.getHat(avatar));
    } else {
      svg = svg.replace('<hat>', '');
    }
    if (this.getGlasses(avatar)) {
      svg = svg.replace('<glasses>', this.getGlasses(avatar));
    } else {
      svg = svg.replace('<glasses>', '');
    }
    return svg;
  }

  getGlasses(avatar: Avatar): string {
    const glasses = avatar.wornOutfits.find(outfit => outfit.type === 'glasses');
    return glasses ? glasses.svg : null;
  }

  getHat(avatar: Avatar): string {
    const hat = avatar.wornOutfits.find(outfit => outfit.type === 'hat');
    return hat ? hat.svg : null;
  }

}
