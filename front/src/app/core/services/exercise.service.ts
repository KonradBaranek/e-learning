import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map, pluck} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class ExerciseService {
  private baseUrl = `${environment.apiUrl}/exercise/`;
  constructor(private http: HttpClient) {}

  start(id: string): Observable<Exercise> {
    return this.http.patch<any>(this.baseUrl + 'start/' + id, {}).pipe(
      pluck('data')
    );
  }

  answer(id: string, answer): Observable<Exercise> {
    return this.http.patch<any>(this.baseUrl + 'answer/' + id, { answer }).pipe(
      pluck('data')
    );
  }

  get(id: string): Observable<Exercise> {
    return this.http.get<any>(this.baseUrl + id).pipe(
      pluck('data')
    );
  }

  list(sort: string, sortDir: 'asc' | 'desc'): Observable<Exercise[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir
      }
    }).pipe(
      pluck('data')
    );
  }

  listBySection(sort: string, sortDir: 'asc' | 'desc', section: string): Observable<Exercise[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir, section
      }
    }).pipe(
      pluck('data')
    );
  }

  listByCreator(sort: string, sortDir: 'asc' | 'desc', creator: string): Observable<Exercise[]> {
    return this.http.get<any>(this.baseUrl, {
      params: {
        sort, dir: sortDir, creator
      }
    }).pipe(
      pluck('data')
    );
  }

  create(exercise: Partial<Exercise>): Observable<Exercise> {
    return this.http.post<any>(this.baseUrl, exercise).pipe(
      pluck('data')
    );
  }

  check(exercise: Partial<Exercise>): Observable<Exercise> {
    return this.http.post<any>(this.baseUrl + 'check/', exercise).pipe(
      pluck('data')
    );
  }

  rate(id: string, positive: boolean): Observable<Exercise> {
    return this.http.patch<any>(this.baseUrl + 'rate/' + id , { positive }).pipe(
      pluck('data')
    );
  }

  edit(id: string, exercise: Partial<Exercise>): Observable<Exercise> {
    return this.http.patch<any>(this.baseUrl + 'edit/' + id , {
      name: exercise.name,
      description: exercise.description,
      section: exercise.section,
      variables: exercise.variables,
      answers: exercise.answers,
    }).pipe(
      pluck('data')
    );
  }

  delete(id: string): Observable<Exercise> {
    return this.http.delete<any>(this.baseUrl + 'delete/' + id).pipe(
      pluck('data')
    );
  }

  progress(id: string, page: number): Observable<Exercise> {
    return this.http.patch<any>(this.baseUrl + 'progress/' + id , { page }).pipe(
      pluck('data')
    );
  }
}
