import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map, pluck} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class SectionService {
  private baseUrl = `${environment.apiUrl}/section/`;
  constructor(private http: HttpClient) {}

  get(id: string): Observable<Section> {
    return this.http.get<any>(this.baseUrl + id).pipe(
      pluck('data')
    );
  }

  list(): Observable<Section[]> {
    return this.http.get<any>(this.baseUrl).pipe(
      pluck('data')
    );
  }

  create(section: Section): Observable<Section> {
    return this.http.post<any>(this.baseUrl, section).pipe(
      pluck('data')
    );
  }

  listGrades(): Observable<Grade[]> {
    return this.list().pipe(map(sections => {
      return sections.reduce((grades, section) => {
        const grade = grades.find(g => g.id === section.grade);
        if (grade) {
          grade.sections.push(section);
        } else {
          grades.push({
            id: section.grade,
            sections: [section]
          });
        }
        return grades;
      }, []).sort((a, b) => a.id - b.id);
    }));
  }
}
