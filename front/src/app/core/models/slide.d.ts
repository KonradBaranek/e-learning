interface Slide {
  title: string;
  description: string;
  showImage: boolean;
  imgPath: string;
  showQuiz: boolean;
  qTitle: string;
  qA1: string;
  qA2: string;
  qA3: string;
  qA4: string;
  showInter: boolean;
  iTitle: string;
  imgPath1: string;
  imgPath2: string;
  imgPath3: string;
  imgPath4: string;
}
