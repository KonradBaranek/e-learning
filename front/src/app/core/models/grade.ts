export class GradeProgress {
  value: number;
  maxValue: number;
}

export class Slide {
  title = '';
  description = '';
  showImage = false;
  imgPath = '';
  showQuiz = false;
  qTitle = '';
  qA1 = '';
  qA2 = '';
  qA3 = '';
  qA4 = '';
  showInter = false;
  iTitle = '';
  imgPath1 = '';
  imgPath2 = '';
  imgPath3 = '';
  imgPath4 = '';
}

export class Presentation {
  id: string;
  slides: Slide[];
  progress?: GradeProgress;
}

export class Exercise {
  id: string;
  description: string;
  answers: string[];
}

export class Lesson {
  id: string;
  name: string;
  progress: GradeProgress;
  presentation: Presentation;
  exercises: Exercise[];
}

export class Section {
  id: string;
  name: string;
  progress: GradeProgress;
  lessons: Lesson[];
}

export class Grade {
  id: string;
  name: string;
  progress: GradeProgress;
  sections: Section[];
}

export class Avatar {
  svg: string;
  hat: any;
  glasses: any;
  color: string;
}


class User {
  email: string;
}

export class UserProgress extends User {
  grades: Grade[];
  avatar: Avatar;
  presentations?: Presentation[];
}

