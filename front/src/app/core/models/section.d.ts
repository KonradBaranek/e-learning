interface Section {
  _id: string;
  name: string;
  description: string;
  grade: number;
}
