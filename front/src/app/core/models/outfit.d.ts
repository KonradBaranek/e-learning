interface Outfit {
  _id: string;
  type: string;
  name: string;
  price: number;
  svg: string;
  state: string;
}
