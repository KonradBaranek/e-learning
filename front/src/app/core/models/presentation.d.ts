interface Presentation {
  _id?: string;
  name: string;
  slides: Partial<Slide>[];
  section: string;
  creator: any;
  ratings: { positive: boolean, user: string }[];
  done?: boolean;
}
