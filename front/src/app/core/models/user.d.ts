interface User {
  _id: string;
  email: string;
  password: string;
  username: string;
  role?: string;
  token?: string;
  progress: any;
  ranking: any;
}
