interface Exercise {
  _id?: string;
  name?: string;
  description: string;
  answers: any[];
  variables: any[];
  section: string;
  creator: any;
  creation: Date;
  lastEdit: Date;
  ratings: { positive: boolean, user: string }[];
  correct?: any;
  isCorrect: boolean;
  done?: boolean;
}
