interface Avatar {
  _id: string;
  name: string;
  ownedOutfits: Outfit[];
  wornOutfits: Outfit[];
  user: string;
  svg: string;
}
