import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { ProgressService } from 'src/app/progress.service';
import { svgMock } from './mock-svg';

const hats = localStorage.getItem('_hats') ? JSON.parse(localStorage.getItem('_hats')) : [{
  id: '1asd',
  type: 'hat',
  price: 400,
  owned: false,
  name: 'Melonik',
  svg: svgMock.hats[0]
},
{
  id: '123asd',
  type: 'hat',
  price: 100,
  owned: false,
  name: 'Cool czapka',
  svg: svgMock.hats[1]
},
{
  id: '123asd',
  type: 'hat',
  price: 0,
  owned: true,
  name: 'Brak',
  svg: ``
}];

const glasses = localStorage.getItem('_glasses') ? JSON.parse(localStorage.getItem('_glasses')) : [{
  id: 'asd',
  type: 'glasses',
  price: 100,
  owned: false,
  name: 'Okulary wiedzy',
  svg: svgMock.glasses[0]
},
{
  id: '13asd',
  type: 'glasses',
  price: 200,
  owned: false,
  name: 'Lemonki',
  svg: svgMock.glasses[1]
},
{
  id: '123asd',
  type: 'glasses',
  price: 300,
  owned: true,
  name: 'RGB',
  svg: svgMock.glasses[2]
},
{
  id: '123as',
  type: 'glasses',
  price: 400,
  owned: true,
  name: 'Soczewki',
  svg: ``
}];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  constructor(private auth: AuthenticationService, private progress: ProgressService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authHeader = request.headers.get('Authorization');
    const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');

    if (request.url.endsWith('/buyItem') && request.method === 'POST') {
      if (request.body.type === 'hat') {
        hats.find(e => e.id === request.body.id).owned = true;
        localStorage.setItem('_hats', JSON.stringify(hats));
        return ok(hats);
      }
      if (request.body.type === 'glasses') {
        glasses.find(e => e.id === request.body.id).owned = true;
        localStorage.setItem('_glasses', JSON.stringify(glasses));
        return ok(glasses);
      }
    }
    if (request.url.endsWith('/equipItem') && request.method === 'POST') {
      if (request.body.type === 'hat') {
        const avatar = this.progress.getAvatar();
        avatar.hat = request.body;
        this.progress.updateAvatar(avatar);
        return ok(avatar);
      }
      if (request.body.type === 'glasses') {
        const avatar = this.progress.getAvatar();
        avatar.glasses = request.body;
        this.progress.updateAvatar(avatar);
        return ok(avatar);
      }
    }
    if (request.url.endsWith('/hats') && request.method === 'GET') {
      return ok(hats);
    }

    if (request.url.endsWith('/avatar') && request.method === 'GET') {
      return ok(this.progress.getAvatar());
    }

    if (request.url.endsWith('/glasses') && request.method === 'GET') {
      return ok(glasses);
    }

    // pass through any requests not handled above
    return next.handle(request);

    // private helper functions

    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function unauthorised() {
      return throwError({ status: 401, error: { message: 'Unauthorised' } });
    }

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }
  }
}

export let fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
