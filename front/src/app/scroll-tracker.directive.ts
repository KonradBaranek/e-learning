import { Directive, ElementRef, HostListener, Input } from '@angular/core';

declare var ga: any;

@Directive({
  selector: '[appScrollTracker]',
  exportAs: 'ScrollTrackerDirective',
})
export class ScrollTrackerDirective {
  reached = false;

  constructor(public el: ElementRef) { }

  @Input('appScrollTracker') option:any;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const elementPosition = this.el.nativeElement.offsetTop;
    const scrollPosition = window.pageYOffset;

    if (this.reached == false) {
      this.reached = scrollPosition >= elementPosition;
      if (this.reached == true) {
        console.log(this.reached);
        console.log(this.el.nativeElement);
        (<any>window).ga('send', 'event', this.option.category, this.option.action, {
          hitCallback: function() {
    
            console.log('Tracking is successful');
          }
        });
      }
    }
  }
}