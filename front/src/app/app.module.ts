import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { KatexModule } from 'ng-katex';

import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ErrorInterceptor, fakeBackendProvider, JwtInterceptor } from './core/helpers';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { SectionComponent } from './section/section.component';
import { LessonComponent } from './lesson/lesson.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfileComponent } from './profile/profile.component';
import { PresentationEditorComponent } from './presentation-editor/presentation-editor.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { ExerciseViewComponent } from './exercise-view/exercise-view.component';
import { SummaryComponent } from './summary/summary.component';
import { PresentationComponent } from './presentation/presentation.component';
import { SlideComponent } from './slide/slide.component';
import { ShopComponent } from './shop/shop.component';
import { ProgressService } from './progress.service';
import { EscapeSVGPipe } from './svgPipe';
import { MyPresentationsComponent } from './my-presentations/my-presentations.component';
import { EventTrackerDirective } from './event-tracker.directive';
import { WorksheetsComponent } from './worksheets/worksheets.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookieDisclaimerComponent } from './cookie-disclaimer/cookie-disclaimer.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { GroupsComponent } from './groups/groups.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { AuthenticationService } from './core/services/authentication.service';
import { PresentationService } from './core/services/presentation.service';
import { SectionService } from './core/services/section.service';
import { PresentationCardComponent } from './presentation-card/presentation-card.component';
import { ExerciseCardComponent } from './exercise-card/exercise-card.component';
import { ExerciseService } from './core/services/exercise.service';
import { ExerciseEditorComponent } from './exercise-editor/exercise-editor.component';
import { ScrollTrackerDirective } from './scroll-tracker.directive';
import { AvatarService } from './core/services/avatar.service';
import { ShopService } from './core/services/shop.service';
import { AvatarPageComponent } from './avatar-page/avatar-page.component';
import { EquationRenderComponent } from './equation-renderer/equation-renderer.component';
import { PresentationListComponent } from './presentation-list/presentation-list.component';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WelcomePageComponent,
    HomeComponent,
    RegisterComponent,
    SectionComponent,
    LessonComponent,
    ProfileComponent,
    PresentationEditorComponent,
    ExerciseComponent,
    ExerciseViewComponent,
    SummaryComponent,
    PresentationComponent,
    SlideComponent,
    ShopComponent,
    EscapeSVGPipe,
    HelpComponent,
    MyPresentationsComponent,
    EventTrackerDirective,
    WorksheetsComponent,
    PrivacyPolicyComponent,
    CookieDisclaimerComponent,
    CookiePolicyComponent,
    GroupsComponent,
    TermsOfUseComponent,
    PresentationCardComponent,
    ExerciseCardComponent,
    ExerciseEditorComponent,
    ScrollTrackerDirective,
    PresentationCardComponent,
    EquationRenderComponent,
    PresentationListComponent,
    AvatarPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    FontAwesomeModule,
    KatexModule
  ],
  providers: [
    ProgressService,
    AuthenticationService,
    PresentationService,
    ExerciseService,
    SectionService,
    AvatarService,
    ShopService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    // mock
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
