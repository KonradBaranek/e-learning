import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SectionService } from '../core/services/section.service';
import { ExerciseService } from '../core/services/exercise.service';
import { Subject, interval } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';

declare var ga: any;

@Component({
  selector: 'app-exercise-editor',
  templateUrl: './exercise-editor.component.html',
  styleUrls: ['./exercise-editor.component.scss']
})
export class ExerciseEditorComponent implements OnInit, OnDestroy {
  exercise: Partial<Exercise> = {
    variables: [],
    answers: [],
    description: ''
  };
  previewExercise = this.exercise;
  grades: Grade[];
  destroy$ = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private exerciseService: ExerciseService,
    private sectionService: SectionService
  ) {}

  ngOnInit() {
    this.sectionService.listGrades().subscribe(grades => {
      this.grades = grades;
      this.exercise.section = grades[0].sections[0].name;
    });
    this.route.params.subscribe(params => {
      const exerId = params['exerciseId'];
      if (exerId) {
        this.exerciseService.get(exerId).subscribe(exercise => this.exercise = exercise);
      }
    });
    interval(1000).pipe(startWith(0), takeUntil(this.destroy$)).subscribe(() => {
      this.exerciseService.check(this.exercise).subscribe(exercise => {
        this.previewExercise = exercise;
      }, () => {
        this.previewExercise = this.exercise;
      });
    });
  }

  saveExercise() {
    if (this.exercise._id) {
      this.exerciseService.edit(this.exercise._id, this.exercise).subscribe(exercise => {
        this.router.navigateByUrl(`/exercise-editor/${this.exercise._id}`);
      });
      (<any>window).ga('send', 'event', "ExcerciseEditor", "Edit-"+this.exercise.section+"-"+this.exercise.name);
    } else {
      this.exerciseService.create(this.exercise).subscribe(exercise => {
        this.router.navigateByUrl(`/exercise-editor/${exercise._id}`);
      });
      (<any>window).ga('send', 'event', "ExcerciseEditor", "Create-"+this.exercise.section+"-"+this.exercise.name);
    }
  }

  createExercise() {
    if (this.exercise._id) {
      this.exerciseService.edit(this.exercise._id, this.exercise).subscribe(exercise => {
        this.router.navigateByUrl(`/my-presentations?tab=exercises`);
      });
      (<any>window).ga('send', 'event', "ExcerciseEditor", "Edit&Send-"+this.exercise.section+"-"+this.exercise.name);
    } else {
      this.exerciseService.create(this.exercise).subscribe(exercise => {
        this.router.navigateByUrl(`/my-presentations?tab=exercises`);
      });
      (<any>window).ga('send', 'event', "ExcerciseEditor", "Create&Send-"+this.exercise.section+"-"+this.exercise.name);
    }
  }

  addAnswer() {
    this.exercise.answers.push({
      description: ''
    });
  }

  addVariable() {
    this.exercise.variables.push({
      name: '',
      exp: '',
      type: 'number'
    });
  }

  deleteAnswer(index: number) {
    this.exercise.answers.splice(index, 1);
  }

  deleteVariable(index: number) {
    this.exercise.variables.splice(index, 1);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
