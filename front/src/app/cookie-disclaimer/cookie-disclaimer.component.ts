import { Component, OnInit } from '@angular/core';
import Cookie from 'js-cookie';

@Component({
  selector: 'app-cookie-disclaimer',
  templateUrl: './cookie-disclaimer.component.html',
  styleUrls: ['./cookie-disclaimer.component.scss']
})
export class CookieDisclaimerComponent implements OnInit {
  isActive = false

  constructor() { }

  ngOnInit() {
    if (Cookie.get('StandardCookieName') !== undefined) {
      this.isActive = false
    } else {
      this.isActive = true
    }
  }

  acceptAllCookie() {
    Cookie.set('StandardCookieName','true')
    Cookie.set('FunctionalCookieName','true')
    Cookie.set('TargetingCookieName','true')
    this.isActive = false;
  }

  closeDisclaimer() {
    Cookie.set('StandardCookieName','Standard cookie value')
    this.isActive = false;
  }
}
