import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

declare var ga: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'E-learnig';
  showFooter = true;
  showHeader = true;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          ga('send', 'pageview', location.pathname);
          const showRegex = /^\/(register|login|privacy-policy|cookie-policy|)([\/#?]?|[\/#?]+.*)$/i;
          this.showFooter = !event.url.match(showRegex);
          this.showHeader = !event.url.match(showRegex);
        }
      });
  }

  onActivate(event) {
    window.scroll(0,0);
  }
}
