import { Component, OnInit } from '@angular/core';
import { ProgressService } from '../progress.service';
import { ShopService } from '../core/services/shop.service';
import { AvatarService } from '../core/services/avatar.service';
import { AuthenticationService } from '../core/services/authentication.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  avatar: Avatar;
  hats: Outfit[];

  glasses: Outfit[];

  user;

  active = true;
  minimize = false;

  constructor(private shopService: ShopService, private avatarService: AvatarService, private authService: AuthenticationService ) {
    this.user = this.authService.currentUserValue;
  }

  ngOnInit() {
    this.update();
  }

  update() {
    this.avatarService.get().subscribe((data: Avatar) => {
      this.avatar = data;

      this.shopService.list().subscribe((data: Outfit[]) => {
        this.updateItems(data);
      });
    });
  }

  buyItem(item) {
    this.shopService.buy(item._id).subscribe(data => {
      this.user.progress.money = data.money;
      this.update();
    });
  }

  equipItem(item) {
    this.avatarService.wear(item._id).subscribe((data: Outfit[]) => {
      this.update();
    });
  }

  updateItems(data) {
    this.glasses = data.filter(outfit => outfit.type === 'glasses').map(outfit => {
      outfit.state = this.avatar.wornOutfits.some(o => o._id === outfit._id) ?
        'worn' : this.avatar.ownedOutfits.some(o => o._id === outfit._id) ?
          'owned' : 'toBuy';
      return outfit;
    });
    this.hats = data.filter(outfit => outfit.type === 'hat').map(outfit => {
      outfit.state = this.avatar.wornOutfits.some(o => o._id === outfit._id) ?
        'worn' : this.avatar.ownedOutfits.some(o => o._id === outfit._id) ?
          'owned' : 'toBuy';
      return outfit;
    });
  }

  toggleActive() {
    this.active = !this.active;
  }

  toggleMinimize() {
    this.minimize = !this.minimize;
  }
}
