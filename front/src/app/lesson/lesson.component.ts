import {Component, OnInit} from '@angular/core';
import {max} from 'rxjs/operators';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';
import { Lesson, Grade, Section } from '../core/models/grade';
import { ProgressService } from '../progress.service';

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent implements OnInit {
  grade: Grade;
  section: Section;
  lesson: Lesson;

  constructor(private route: ActivatedRoute, private dataService: DataService, private progress: ProgressService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dataService.getLesson(params['grade'], params['section'], params['lesson']).subscribe(lesson => {
        this.lesson = lesson;
      });
      this.dataService.getSection(params['grade'], params['section']).subscribe(section => {
        this.section = section;
      });
      this.dataService.getGrade(params['grade']).subscribe(grade => {
        this.grade = grade;
      });
    });
  }

  test() {
    this.progress.updatePresentationProgress(this.grade, this.section, this.lesson, this.lesson.presentation.progress.value + 1);
  }
}
