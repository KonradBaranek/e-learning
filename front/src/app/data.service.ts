import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import { Lesson, Section, Grade, Presentation } from './core/models/grade';

const mock: Partial<Exercise>[] = [{
  _id: '0',
  description: 'Wybierz zdanie fałszywe:',
  answers: [
    'Każdy prostokąt jest kwadratem.',
    'Każdy kwadrat ma wszystkie boki równej długości.',
    'Każdy prostokąt ma dwie pary boków równoległych.',
    'Każdy kwadrat jest prostokątem.'
  ]
}, {
  _id: '1',
  description: 'Który z wielokątów opisanych długościami boków to prostokąt?',
  answers: [
    '37 cm, 2 dm, 37 cm, 0,2 m',
    '22 cm, 5 cm, 22 cm',
    '1 m, 83 cm, 7 dm, 83 cm',
    '53 cm, 35 cm, 53 cm, 12 cm',
  ]
}, {
  _id: '2',
  description: 'Obwód prostokąta o bokach 25 cm i 35 cm wynosi:',
  answers: [
    '120 cm',
    '1 m',
    '1,2 m',
    '240 cm'
  ]
}, {
  _id: '3',
  description : 'Wybierz parę figur, w której obwód kwadratu jest większy od obwodu prostokąta.',
  answers: [
    'Prostokąt o bokach 44 cm i 6 cm, kwadrat o bokach 40 cm',
    'Prostokąt o bokach 12 cm i 8 cm, kwadrat o bokach 10 cm',
    'Prostokąt o bokach 20 cm i 3 dm, kwadrat o bokach 24 cm',
    'Prostokąt o bokach 25 cm i 0,2 m, kwadrat o bokach 2 dm'
  ]
}, {
  _id: '4',
  description: 'Obwód kwadratu musi być liczbą podzielną przez:',
  answers: [
    '4',
    '3',
    '7',
    '5'
  ]
}, {
  _id: '5',
  description: 'Wybierz parę boków, której nie może mieć prostokąt o obwodzie 46 cm.',
  answers: [
    '10cm, 16 cm',
    '22 cm, 1 cm',
    '8 cm, 15 cm',
    '2 dm, 3 cm'
  ]
}, {
  _id: '6',
  description: 'Wybierz parę figur których obwód jest równy.',
  answers: [
    'prostokąt o bokach 20 cm i 3 dm, oraz kwadrat o bokach 25 cm',
    'prostokąty o bokach 5 cm i 12 cm, oraz 10 cm i 6 cm',
    'prostokąty o bokach 32 cm i 42 cm, oraz 20 cm i 54 cm',
    'prostokąt o bokach 15 cm i 18 cm, oraz kwadrat o bokach 16 cm'
  ]
}];

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  public buyItem(item: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/buyItem`, item);
  }

  public equipItem(item: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/equipItem`, item);
  }

  public getHats(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/hats`);
  }

  public getGlasses(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/glasses`);
  }

  // TODO service for profile info
  public getProfile() {

  }

  public getProgress(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/progress`);
  }

  // TODO service for lessons
  public getGrades(): Observable<Grade[]> {
    return this.http.get<Grade[]>(`${environment.apiUrl}/grades`);
  }

  public getGrade(grade: string): Observable<Grade> {
    return this.http.get<Grade>(`${environment.apiUrl}/grade/`, {
      params: {
        grade
      }
    });
  }

  public getSection(grade: string, section: string): Observable<Section> {
    return this.http.get<Section>(`${environment.apiUrl}/section/`, {
      params: {
        grade, section
      }
    });
  }

  public getLesson(grade: string, section: string, _id: string): Observable<Lesson> {
    return this.http.get<Lesson>(`${environment.apiUrl}/lesson/`, {
      params: {
        grade, section, _id
      }
    });
  }

  public getExercise(_id: number): Partial<Exercise> {
    const ex = mock[_id % mock.length];
    // const answers = [...ex.answers];
    const answers = null;
    const ans = [];
    while (answers.length) {
      const rand = Math.floor(Math.random() * answers.length);
      ans.push(answers[rand]);
      answers.splice(rand, 1);
    }
    return {
      _id: ex._id,
      description: ex.description,
      answers: ans
    };
  }

  public checkExercise(_id: number, answer: string): boolean {
    const ex = mock[_id % mock.length];
    return ex.answers[0] === answer;
  }

  public postPresentation(_id: string, presentation: Presentation) {
    return this.http.post(`${environment.apiUrl}/presentation/create/`, presentation, {
      params: {
        _id
      }
    });
  }

  public getPresentation(_id: string) {
    return this.http.get<Presentation>(`${environment.apiUrl}/presentation/`, {
      params: {
        _id
      }
    });
  }
}
