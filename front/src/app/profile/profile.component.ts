import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from '../data.service';
import { ProgressService } from '../progress.service';
import { AvatarService } from '../core/services/avatar.service';
import { AuthenticationService } from '../core/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

  // TODO make model
  user: User;

  loading = false;

  avatar: Avatar;

  svg: String;

  constructor(private avatarService: AvatarService, private authService: AuthenticationService) {

  }

  ngOnInit() {
    this.user = this.authService.currentUserValue;

    this.avatarService.get().subscribe(data => {
      this.avatar = data;
    });
  }


}
