import {Component, OnInit} from '@angular/core';
import { SectionService } from '../core/services/section.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  grades: Grade[];
  constructor(private sectionService: SectionService) {}

  ngOnInit() {
    this.sectionService.listGrades().subscribe(grades => this.grades = grades);
  }

}
