import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Grade } from '../core/models/grade';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PresentationService } from '../core/services/presentation.service';
import { SectionService } from '../core/services/section.service';
import { ExerciseService } from '../core/services/exercise.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private presentationService: PresentationService,
    private exerciseService: ExerciseService,
    private sectionService: SectionService
  ) {}

  presentations: Presentation[];
  exercises: Exercise[];
  grade: Grade;
  section: Section;
  tabIndex: number;
  private tabNames = ['presentations', 'exercises'];

  ngOnInit() {
    combineLatest(this.route.params, this.route.queryParams).pipe(take(1))
    .subscribe(([params, queryParams]) => {
      this.sectionService.get(params['section'])
      .subscribe((section) => {
        this.section = section;
        this.presentationService.listBySection('name', 'asc', section.name)
        .subscribe(presentations => this.presentations = presentations);
        this.exerciseService.listBySection('name', 'asc', section.name)
        .subscribe(exercises => this.exercises = exercises);
      });

      this.changeTab(queryParams['tab']);
    });
  }

  changeTab(tab: 'presentations' | 'exercises' | number) {
    if (typeof tab === 'string') {
      this.tabIndex = this.tabNames.indexOf(tab);
      if (this.tabIndex < 0) {
        this.tabIndex = 0;
      }
    } else {
      this.tabIndex = tab;
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { tab: this.tabNames[this.tabIndex] },
    });
  }

  ratePresentation(id: string, positive: boolean) {
    this.presentationService.rate(id, positive).subscribe(presentation => {
      const pres = this.presentations.findIndex(p => p._id === presentation._id);
      this.presentations[pres] = presentation;
    });
  }

  deletePresentation(id: string) {
    this.presentationService.delete(id).subscribe(pres => {
      this.presentations = this.presentations.filter(p => p._id !== pres._id);
    });
  }

  editPresentation(id: string) {
    this.router.navigate(['/presentation-editor', id]);
  }

  rateExercise(id: string, positive: boolean) {
    this.exerciseService.rate(id, positive).subscribe(exercise => {
      const pres = this.exercises.findIndex(p => p._id === exercise._id);
      this.exercises[pres] = exercise;
    });
  }

  deleteExercise(id: string) {
    this.exerciseService.delete(id).subscribe(pres => {
      this.exercises = this.exercises.filter(p => p._id !== pres._id);
    });
  }

  editExercise(id: string) {
    this.router.navigate(['/exercise-editor', id]);
  }
}
