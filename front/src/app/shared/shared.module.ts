// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Material
import {
  MatButtonModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSidenavModule,
  MatMenuModule,
  MatTabsModule,
  MatSelectModule,
  MatIconModule,
} from '@angular/material';
// Modified components
import { ButtonBaseComponent } from './components/button-base/button-base.component';
import { CardBaseComponent } from './components/card-base/card-base.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RoleDirective } from './directives/roles.directive';

import {CarouselComponent} from './components/carousel/carousel.component';
import { EscapeHtmlPipe } from './pipes/escapeHtmlPipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatMenuModule,
    MatTabsModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
  ],
  declarations: [
    ButtonBaseComponent,
    CardBaseComponent,
    ProgressBarComponent,
    HeaderComponent,
    FooterComponent,
    CarouselComponent,
    EscapeHtmlPipe,
    RoleDirective,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // material exports
    MatButtonModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatTabsModule,
    MatSelectModule,
    // declarations
    ButtonBaseComponent,
    CardBaseComponent,
    ProgressBarComponent,
    FooterComponent,
    HeaderComponent,
    CarouselComponent,
    MatIconModule,
    MatCheckboxModule,
  ]
})

export class SharedModule {
}

