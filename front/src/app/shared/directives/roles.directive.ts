import { ElementRef, TemplateRef, ViewContainerRef, Directive, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Directive({
  selector: '[appRole]'
})
export class RoleDirective implements OnInit {
  private currentUser;
  private roles: string | string[];

  @Input()
  set appRole(val: string | string[]) {
    this.roles = val;
    this.updateView();
  }

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.authService.currentUser.subscribe(user => {
      this.currentUser = user;
      this.updateView();
    });
  }

  private updateView() {
    if (this.checkPermission()) {
        this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private checkPermission() {
    if (this.currentUser && this.currentUser.role) {
      if (Array.isArray(this.roles)) {
        return this.roles.some((role: string) => role.toLocaleLowerCase() === this.currentUser.role.toLocaleLowerCase());
      }
      return this.roles.toLocaleLowerCase() === this.currentUser.role.toLocaleLowerCase();
    }

    return false;
  }
}
