import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isActive = false;

  toggleCollapse() {
    this.isActive = !this.isActive;
  }

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isActive = false;
      }
    });
  }

  logout() {
    this.auth.logout();
    this.router.navigateByUrl('');
  }

}
