import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShopService } from 'src/app/core/services/shop.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})

// TODO change to universal component
export class CarouselComponent implements OnInit {

  @Input() items: [];
  @Output() buy = new EventEmitter();
  @Output() equip = new EventEmitter();

  constructor(private shopService: ShopService) { }

  ngOnInit() {
  }

  equipEvent(item) {
    this.equip.emit(item);
  }

  buyEvent(item) {
    this.buy.emit(item);
  }

  cutItems(items = []) {
    return items.slice(0, 3);
  }

  moveLeft() {
    this.items.unshift(this.items.splice(this.items.length - 1, 1)[0]);
  }

  moveRight() {
    this.items.push(this.items.splice(0, 1)[0]);
  }

}
