import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Slide } from '../core/models/grade';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss']
})
export class SlideComponent implements OnChanges {
  @Input() slide: Slide;
  @Input() pageNumber: number;
  @Input() maxPages: number;
  revealedAnswers = false;
  revealedPhotos = false;
  order = [1, 2, 3, 4];

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.slide) {
      this.newSlide();
    }
  }

  newSlide() {
    const newOrder = Array(4).fill(0).map(() => Math.floor(Math.random() * 4));
    this.order = newOrder;
    this.revealedPhotos = false;
    this.revealedAnswers = false;
  }

  revealAnswers() {
    this.revealedAnswers = true;
  }

  revealPhotos() {
    this.revealedPhotos = true;
  }
}
