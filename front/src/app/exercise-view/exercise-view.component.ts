import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExerciseService } from '../core/services/exercise.service';

@Component({
  selector: 'app-exercise-view',
  templateUrl: './exercise-view.component.html',
  styleUrls: ['./exercise-view.component.scss']
})
export class ExerciseViewComponent implements OnInit {
  exercise: Exercise;
  exerciseId: string;

  constructor(private route: ActivatedRoute, private exerciseService: ExerciseService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.exerciseId = params['exercise'];
      this.exerciseService.start(this.exerciseId).subscribe(exercise => {
        this.exercise = exercise;
      });
    });
  }

  chooseAnswer(answer: string) {
    if (this.exercise.correct) {
      return;
    }
    this.exerciseService.answer(this.exerciseId, answer).subscribe(exercise => {
      this.exercise = exercise;
    });
  }
}
