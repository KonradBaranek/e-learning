import { Grade, UserProgress } from './app/core/models/grade';
import * as mock from './mock';

interface Storage {
  users: UserProgress[];
}

const standardGrades: Grade[] = [
  mock.grade,
  mock.grade,
  mock.grade
];

const storage: Storage = {
  users: [{
    ...mock.user,
    grades: standardGrades,
    avatar: mock.avatar
  }]
};

export const loadStorage = () => {
  Object.entries(storage).forEach(([key, value]) => {
    if (!localStorage.getItem(key)) {
      localStorage.setItem(key, JSON.stringify(value));
    }
  });
};

export const clearStorage = () => {
  localStorage.clear();
};

export const reloadStorage = () => {
  clearStorage();
  loadStorage();
};

Object.defineProperties(window, {
  loadStorage: {
    value: loadStorage
  },
  clearStorage: {
    value: clearStorage
  },
  reloadStorage: {
    value: reloadStorage
  },
  getStorage: {
    value: () => JSON.parse(localStorage.getItem('users'))
  }
});
