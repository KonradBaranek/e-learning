import { Grade, Slide, Avatar } from './app/core/models/grade';
import { svgMock } from './app/core/helpers/mock-svg';

export const slide: Slide = {
  title: 'Liczby naturalne',
  description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Aenean volutpat luctus massa, ut lobortis metus malesuada ut.
    Mauris eu purus erat. Quisque a neque non felis convallis gravida.
    Curabitur in auctor enim, nec convallis quam. Donec nec augue a ipsum mollis aliquet.
    Ut malesuada risus mauris, ut ullamcorper ex lacinia et. In placerat maximus venenatis.
    Suspendisse vulputate risus turpis, ut blandit odio ultricies quis.`.replace('\n', ' '),
  showImage: true,
  imgPath: 'https://www.wikihow.com/images/thumb/f/f0/Bubble-Numbers.png/340px-Bubble-Numbers.png',
  showQuiz: false,
  qTitle: '',
  qA1: '',
  qA2: '',
  qA3: '',
  qA4: '',
  showInter: false,
  iTitle: '',
  imgPath1: '',
  imgPath2: '',
  imgPath3: '',
  imgPath4: ''
};

export const grade: Grade = {
  id: 'class1',
  name: 'klasa 1',
  progress: {
    value: 1,
    maxValue: 10
  },
  sections: [{
    id: 'liczby-naturalne',
    name: 'Czworoboki',
    progress: {
      value: 4,
      maxValue: 10
    },
    lessons: [{
      id: 'Prostokaty-i-kwadraty',
      name: 'Prostokąty i kwadraty',
      progress: {
        value: 2,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [{
          title: 'Prostokąt',
          description: 'Prostokąt ma wszystkie kąty proste oraz dwie pary boków równoległych i są one parami równe',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Kwadrat',
          description: 'Kwadratem nazywamy prostokąt, który ma wszystkie boki równe.',
          showImage: true,
          imgPath: 'https://eszkola.pl/img/works_images/kwadrat.bmp',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Obwód',
          description: 'Obwodem prostokąta nazywamy sumę długości jego wszystkich boków.\nDla prostokąta na obrazku wyraża się on wzorem Obwód = a+a+b+b',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Pole prostokąta',
          description: 'Polem prostokąta nazywamy iloczyn jego podstawy i wysokości.\nDla prostokąta na obrazku jest to Pole = a * b',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        }],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }, {
      id: 'mnozenie',
      name: 'mnożenie',
      progress: {
        value: 0,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [slide],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }, {
      id: 'odejmowanie',
      name: 'odejmowanie',
      progress: {
        value: 0,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [slide],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }]
  }]
};

export const emptyGrade = {
  id: 'class1',
  name: 'klasa 1',
  progress: {
    value: 0,
    maxValue: 10
  },
  sections: [{
    id: 'liczby-naturalne',
    name: 'Czworoboki',
    progress: {
      value: 0,
      maxValue: 10
    },
    lessons: [{
      id: 'Prostokaty-i-kwadraty',
      name: 'Prostokąty i kwadraty',
      progress: {
        value: 0,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [{
          title: 'Prostokąt',
          description: 'Prostokąt ma wszystkie kąty proste oraz dwie pary boków równoległych i są one parami równe',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Kwadrat',
          description: 'Kwadratem nazywamy prostokąt, który ma wszystkie boki równe.',
          showImage: true,
          imgPath: 'https://eszkola.pl/img/works_images/kwadrat.bmp',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Obwód',
          description: 'Obwodem prostokąta nazywamy sumę długości jego wszystkich boków.\nDla prostokąta na obrazku wyraża się on wzorem Obwód = a+a+b+b',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        },
        {
          title: 'Pole prostokąta',
          description: 'Polem prostokąta nazywamy iloczyn jego podstawy i wysokości.\nDla prostokąta na obrazku jest to Pole = a * b',
          showImage: true,
          imgPath: 'https://www.matmana6.pl/zdjecia/szkola_srednia/figury_plaskie_planimetria/prostokat_47.png',
          showQuiz: false,
          qTitle: '',
          qA1: '',
          qA2: '',
          qA3: '',
          qA4: '',
          showInter: false,
          iTitle: '',
          imgPath1: '',
          imgPath2: '',
          imgPath3: '',
          imgPath4: ''
        }],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }, {
      id: 'mnozenie',
      name: 'mnożenie',
      progress: {
        value: 0,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [slide],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }, {
      id: 'odejmowanie',
      name: 'odejmowanie',
      progress: {
        value: 0,
        maxValue: 10
      },
      presentation: {
        id: '#123',
        slides: [slide],
        progress: {
          value: 0,
          maxValue: 10
        }
      },
      exercises: [{
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }, {
        id: '#3',
        description: 'Ala ma kota',
        answers: ['1', '2', '3', '4']
      }]
    }]
  }]
};

export const avatar: Avatar = {
  svg: svgMock.avatar,
  hat: null,
  glasses: null,
  color: 'pink'
};

export const user: any = {
  username: 'test',
  email: 'test',
  password: 'test',
  role: 'admin',
};
