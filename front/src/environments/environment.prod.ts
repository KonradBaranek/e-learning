export const environment = {
  production: false,
  apiUrl: 'https://learningo1.herokuapp.com/api/1',
  mockLogin: true
};
