const expEvaluator = require('math-expression-evaluator')
const { randomInt, randomFloat } = require('./random')

const callRegex = '\\(([()a-zA-Z0-9$_,. -]*)\\)'
const genFunctionRegex = name => new RegExp('\\' + name + ' *' + callRegex)

const builtInFunctions = [
  { name: '$randomInt', fn: randomInt },
  { name: '$randomFloat', fn: randomFloat },
  { name: '$ceil', fn: (num, pre = 0) => Math.ceil(num / (10 ** -pre)) * (10 ** -pre) },
  { name: '$floor', fn: (num, pre = 0) => Math.floor(num / (10 ** -pre)) * (10 ** -pre) },
  { name: '$round', fn: (num, pre = 0) => Math.round(num / (10 ** -pre)) * (10 ** -pre) }
].map(e => Object.assign(e, { regex: genFunctionRegex(e.name) }))

const builtInVariables = [

]

// replace calculated variables with values
const replaceVariables = (string, variables) => {
  return variables.reduce((str, { name, value }) => {
    const reg = new RegExp(`\\${name}`, 'g')
    return str.replace(reg, value)
  }, string)
}

const evaluateVariables = (exp, variables, callStack) => {
  if (callStack < 0) {
    throw new Error('Maximum call stack exceed.')
  }
  while (true) {
    // find used function
    const best = builtInFunctions.reduce((acc, func) => {
      const result = exp.match(func.regex)
      if (!result) {
        return acc
      }
      if (result.index < acc.result.index) {
        return { func, result }
      } else {
        return acc
      }
    }, { func: null, result: { index: exp.length } })
    // if no function used break
    if (!best.func) {
      break
    }
    // calculated function value
    const call = best.result[0]
    const argsString = best.result[1]
    const result = evaluateVariables(argsString, variables, callStack - 1)
    const args = result.split(/ *, */g).map(e => expEvaluator.eval(e))
    const functionEval = best.func.fn(...args)
    exp = exp.replace(call, functionEval)
  }
  exp = replaceVariables(exp, builtInVariables)
  exp = replaceVariables(exp, variables)
  return exp
}

const characters = 'abcdefghijklmnopqrstuvwxyz'.split('')

const randomCharacter = (except = '') => {
  const newLetters = characters.filter(e => !except.includes(e))
  return newLetters[randomInt(0, newLetters.length - 1)]
}

const props = {
  variables: Symbol('variables'),
  usedLetters: Symbol('usedLetters'),
  lastLetterIndex: Symbol('lastLetterIndex')
}

class Evaluator {
  constructor (variables) {
    if (!variables) {
      throw new Error('No variables provided.')
    }
    if (variables.length > 30) {
      throw new Error('Two many variables.')
    }
    if (variables.some(e => (e.exp || {}).length > 300)) {
      throw new Error('Expression too long.')
    }
    if (variables.some(({ name: name1 }, i) => variables.some(({ name: name2 }, j) => i !== j && name1 === name2))) {
      throw new Error('Variables names have to be unique.')
    }
    this[props.variables] = variables
    this[props.usedLetters] = []
    this[props.lastLetterIndex] = -1
  }
  execute (maxCallStack = 10) {
    const vars = this[props.variables].reduce((variables, current) => {
      if (current.type === 'string') {
        let newCharacter
        if (current.random) {
          newCharacter = randomCharacter(this[props.usedLetters])
        } else {
          newCharacter = characters.filter(e => !this[props.usedLetters].includes(e))[++this[props.lastLetterIndex]]
        }
        this[props.usedLetters].push(newCharacter)
        current.value = current.uppercase ? newCharacter.toUpperCase() : newCharacter
        return variables.concat(current)
      } else {
        const newExp = evaluateVariables(current.exp, variables, maxCallStack)
        current.value = expEvaluator.eval(newExp)
        return variables.concat(current)
      }
    }, [])
    return vars
  }
}

module.exports = Evaluator
