const randomFloat = (min, max) => {
  return Math.random() * (max - min) + min
}

const randomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

const shuffle = array => {
  for (let i = 0; i < array.length; i += 1) {
    const ran = randomInt(i, array.length - 1)
    const temp = array[i]
    array[i] = array[ran]
    array[ran] = temp
  }
  return array
}

module.exports = {
  randomFloat,
  randomInt,
  shuffle
}
