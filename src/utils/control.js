const Role = require('../models/RoleModel')

const methods = ['create', 'read', 'update', 'delete']

const parsePrivileges = (privileges) => {
  const parsed = Object.entries(privileges)
  if (parsed.some(p => p[1].some(m => !methods.includes(m)))) {
    throw new Error('Privilege method must be one of there: ' + methods)
  }
  return parsed
}

const can = (privileges) => {
  const parsedPrivileges = parsePrivileges(privileges)
  return async (req, res, next) => {
    const role = await Role.findOne({ name: req.user.role })
    const isEveryTrue = parsedPrivileges.every(([name, list]) => {
      const privilege = role.privileges.find(p => p.name === name)
      return list.every(method => privilege[method])
    })
    if (isEveryTrue) {
      next()
    } else {
      return res.status(401).send('Unauthorized')
    }
  }
}

module.exports = Object.freeze({
  can,
  CREATE: 'create',
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete'
})
