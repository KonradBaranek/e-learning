const ensure = (property, ...subProperties) => (req, res, next) => {
  if (!req[property]) {
    return res.status(400).json({ message: 'Body is required' })
  }
  const values = Object.keys(req[property])
  const validate = subProperties.some(prop => {
    if (Array.isArray(prop)) {
      return prop.every(p => values.includes(p))
    }
    return values.includes(prop)
  })
  if (!validate) {
    return res.status(400).json({ message: subProperties })
  }
  next()
}

module.exports = ensure
