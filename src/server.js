const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const path = require('path')
const passport = require('passport')
const logger = require('./utils/logger')

// set mongoose promise handling
mongoose.Promise = global.Promise
// connect to the database
mongoose.connect(process.env.DATABASE_STRING, { useNewUrlParser: true, useCreateIndex: true })
  .then(() => {
    logger.info('Connected to database')
  })
  .catch(error => {
    logger.error('Database error:', error)
  })

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())

app.use(passport.initialize())
require('../config/passport')

require('express-async-errors')

app.use(express.static(path.join(__dirname, '../', 'front', 'public')))

app.use('/api/1', require('./api/v1'))

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'front', 'public', 'index.html'))
})

// error handling
app.use((err, req, res, next) => {
  res.status(500).send('Internal server error!')
  next(err)
})

module.exports = app
