const express = require('express')
const router = express.Router()
const ensure = require('../utils/ensure')
const passport = require('passport')

const auth = passport.authenticate('jwt', { session: false })

const UserController = require('../controllers/UserController')
const user = express.Router()
user.post('/register', ensure('body', ['email', 'password']), UserController.register)
user.post('/login', ensure('body', ['email', 'password']), UserController.login)
user.patch('/me', auth, UserController.edit)
user.get('/me', auth, UserController.me)

const PresentationController = require('../controllers/PresentationController')
const presentation = express.Router()
presentation.post('/', auth, PresentationController.create)
presentation.get('/:id', auth, PresentationController.findOne)
presentation.get('/', auth, PresentationController.list)
presentation.patch('/progress/:id', auth, PresentationController.makeProgress)
presentation.patch('/rate/:id', auth, PresentationController.rate)
presentation.patch('/edit/:id', auth, PresentationController.edit)
presentation.delete('/:id', auth, PresentationController.remove)

const SectionController = require('../controllers/SectionController')
const section = express.Router()
section.post('/', auth, SectionController.create)
section.get('/:id', auth, SectionController.findOne)
section.get('/', auth, SectionController.list)

const ShopController = require('../controllers/ShopController')
const shop = express.Router()
shop.patch('/:id', auth, ShopController.buy)
shop.get('/:id', auth, ShopController.findOne)
shop.get('/', auth, ShopController.list)

const AvatarController = require('../controllers/AvatarController')
const avatar = express.Router()
avatar.patch('/:id', auth, AvatarController.wear)
avatar.get('/', auth, AvatarController.findForUser)

const ExerciseController = require('../controllers/ExerciseController')
const exercise = express.Router()
exercise.get('/', auth, ExerciseController.list)
exercise.patch('/start/:id', auth, ExerciseController.start)
exercise.patch('/answer/:id', auth, ExerciseController.answer)
exercise.get('/:id', auth, ExerciseController.findOne)
exercise.post('/', auth, ExerciseController.create)
exercise.post('/check', auth, ExerciseController.check)
exercise.patch('/rate/:id', auth, ExerciseController.rate)
exercise.patch('/edit/:id', auth, ExerciseController.edit)
exercise.delete('/:id', auth, ExerciseController.remove)

router.use('/user', user)
router.use('/section', section)
router.use('/presentation', presentation)
router.use('/shop', shop)
router.use('/avatar', avatar)
router.use('/exercise', exercise)

module.exports = router
