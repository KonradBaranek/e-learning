const mongoose = require('mongoose')

const PresentationSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  slides: [{
    title: String,
    description: String,
    showImage: Boolean,
    imgPath: String,
    showQuiz: Boolean,
    qTitle: String,
    qA1: String,
    qA2: String,
    qA3: String,
    qA4: String,
    showInter: Boolean,
    iTitle: String,
    imgPath1: String,
    imgPath2: String,
    imgPath3: String,
    imgPath4: String
  }],
  section: {
    type: String,
    required: true
  },
  creator: {
    type: mongoose.Types.ObjectId,
    ref: 'user',
    required: false
  },
  ratings: [{
    positive: Boolean,
    user: mongoose.Types.ObjectId
  }]
})

const PresentationModel = mongoose.model('presentation', PresentationSchema, 'presentations')

module.exports = {
  PresentationModel
}
