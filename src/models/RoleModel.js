const mongoose = require('mongoose')

const PrivilegeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  create: {
    type: Boolean,
    required: true,
    default: false
  },
  update: {
    type: Boolean,
    required: true,
    default: false
  },
  read: {
    type: Boolean,
    required: true,
    default: false
  },
  delete: {
    type: Boolean,
    required: true,
    default: false
  }
})

const RoleSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  privileges: [PrivilegeSchema]
})

const Role = mongoose.model('role', RoleSchema, 'roles')

module.exports = Role
