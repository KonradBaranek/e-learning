const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const validator = require('validator')
const SALT_WORK_FACTOR = 10

const defaultProgress = {
  money: 0
}

const passwordConstrains = {
  min: 8,
  max: 30,
  specialCharacters: String.raw`! "#$%&'()*+,./:;<=>?@[\]^_{|}~-` + '`'
}

// const specialRegex = passwordConstrains.specialCharacters.split('').map(e => ['\\', ']'].includes(e) ? '\\' + e : e)

const passwordRegex = new RegExp(`.{${passwordConstrains.min},${passwordConstrains.max}}`)
// const passwordRegex = new RegExp(`(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[${specialRegex}]).{${passwordConstrains.min},${passwordConstrains.max}}`)
const passwordMessage = `Password has to be between ${passwordConstrains.min} and ${passwordConstrains.max} characters long and includes at least one:
lowercase letter, uppercase letter, digit and one of these special characters:
[${passwordConstrains.specialCharacters.split('').join(', ')}].`.replace(/\s/g, ' ')

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    minlength: 3,
    maxlength: 16
  },
  firstName: {
    type: String,
    minlength: 1,
    maxlength: 16
  },
  lastName: {
    type: String,
    minlength: 1,
    maxlength: 16
  },
  email: {
    required: true,
    type: String,
    unique: true,
    validate: {
      validator: email => validator.isEmail(email),
      message: 'That is not proper email address.'
    }
  },
  password: {
    required: true,
    type: String,
    validate: {
      validator: pass => passwordRegex.test(pass),
      message: passwordMessage
    }
  },
  creation: { type: Date, default: Date.now, required: true },
  lastLogin: { type: Date, default: null },
  role: {
    type: String,
    enum: ['user', 'teacher', 'editor', 'admin'],
    default: 'user',
    required: true
  },
  provider: {
    type: {
      name: String,
      id: String,
      token: String
    },
    default: null
  },
  progress: {
    type: {},
    default: defaultProgress
  }
})

const hashPassword = async function (next) {
  // only hash the password if it has been modified (or is new)
  if (!this.isModified('password')) {
    return
  }
  // generate a salt
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)
  // hash the password using new salt
  const hash = await bcrypt.hash(this.password, salt)
  // override the cleartext password with the hashed one
  this.password = hash
}

const comparePassword = async function (candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password)
}

const generateToken = function () {
  return jwt.sign(this.toJSON(), process.env.JWT_SECRET, { expiresIn: '1h' })
}

const stripSensitive = function () {
  const clone = Object.assign({}, this)._doc;
  ['password', 'provider', '__v'].forEach(p => Reflect.deleteProperty(clone, p))
  return clone
}

UserSchema.pre('save', hashPassword)

UserSchema.methods.comparePassword = comparePassword
UserSchema.methods.generateToken = generateToken
UserSchema.methods.stripSensitive = stripSensitive

const UserModel = mongoose.model('user', UserSchema, 'users')

module.exports = {
  UserModel
}
