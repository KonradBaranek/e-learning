const mongoose = require('mongoose')
const Evaluator = require('../utils/evaluator')

const ExerciseSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  variables: [{
    name: { type: String, required: true },
    exp: { type: String, required: false },
    type: { type: String, required: true, default: 'number' },
    random: { type: Boolean, required: false, default: false }
  }],
  section: {
    type: String,
    required: true
  },
  answers: [{
    correct: Boolean,
    description: String
  }],
  creator: { type: mongoose.Types.ObjectId, required: true, ref: 'user' },
  creation: { type: Date, default: Date.now, required: true },
  lastEdit: { type: Date, default: null },
  ratings: [{
    positive: Boolean,
    user: mongoose.Types.ObjectId
  }]
})

const evaluate = function () {
  const evaluator = new Evaluator(this._doc.variables)
  const vars = evaluator.execute()
  return vars
}

ExerciseSchema.methods.evaluate = evaluate

const ExerciseModel = mongoose.model('exercise', ExerciseSchema, 'exercises')

module.exports = {
  ExerciseModel
}
