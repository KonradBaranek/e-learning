const mongoose = require('mongoose')

const SectionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  grade: {
    type: Number,
    required: true
  }
})

const SectionModel = mongoose.model('section', SectionSchema, 'sections')

module.exports = {
  SectionModel
}
