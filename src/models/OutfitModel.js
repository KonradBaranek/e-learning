const mongoose = require('mongoose')

const OutfitSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  svg: {
    type: String,
    required: true
  }
})

const OutfitModel = mongoose.model('outfit', OutfitSchema, 'outfits')

module.exports = {
  OutfitModel
}
