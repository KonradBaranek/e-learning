const mongoose = require('mongoose')

const AvatarSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  ownedOutfits: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'outfit' }],
    required: true
  },
  wornOutfits: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'outfit' }],
    required: true
  },
  user: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  svg: {
    type: String,
    required: true
  }
})

const AvatarModel = mongoose.model('avatar', AvatarSchema, 'avatars')

module.exports = {
  AvatarModel
}
