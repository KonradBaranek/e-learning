require('dotenv').config()
const fs = require('fs')
const mongoose = require('mongoose')
const { SectionModel } = require('../models/SectionModel')
const { OutfitModel } = require('../models/OutfitModel')
const { PresentationModel } = require('../models/PresentationModel')
const { UserModel } = require('../models/UserModel')
const { ExerciseModel } = require('../models/ExerciseModel')
const { AvatarModel } = require('../models/AvatarModel');

(async () => {
  await mongoose.connect(process.env.DATABASE_STRING, { useNewUrlParser: true, useCreateIndex: true })

  const deleteData = async (model) => {
    await model.deleteMany({})
  }

  const insertData = async (model, filename) => {
    try {
      await deleteData(model)
      const data = JSON.parse(fs.readFileSync(`./src/setup/${filename}.json`, 'utf-8'))
      await Promise.all(data.map(async d => {
        await model.create(d)
      }))
    } catch (e) {
      console.error('Something went wrong', e)
    }
  }

  await Promise.all([
    deleteData(AvatarModel),
    insertData(SectionModel, 'sections'),
    insertData(OutfitModel, 'outfits'),
    insertData(PresentationModel, 'presentations'),
    insertData(UserModel, 'users'),
    insertData(AvatarModel, 'avatars'),
    insertData(ExerciseModel, 'exercises')
  ])
})()
  .then(() => {
    console.log('Done successfuly')
  })
  .catch(e => {
    console.error('Error occured', e)
  })
  .finally(() => {
    process.exit(0)
  })
