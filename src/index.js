require('dotenv').config()
const logger = require('./utils/logger')

const app = require('./server')

app.listen(process.env.PORT, () => {
  logger.info(`Server is running at ${process.env.PORT}`)
})
