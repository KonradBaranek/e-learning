const { PresentationModel } = require('../models/PresentationModel')
const { UserModel } = require('../models/UserModel')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')
const money = require('../utils/money')

const create = async (req, res) => {
  const presentation = new PresentationModel({
    name: req.body.name,
    section: req.body.section,
    slides: req.body.slides,
    creator: req.user._id
  })

  try {
    await presentation.save()
    req.user.progress.money += money.PRESENTATION_CREATE
    req.user.markModified('progress')
    await req.user.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'Presentation created.', data: presentation })
}

const findOne = async (req, res) => {
  const presentation = await PresentationModel.findById(req.params.id).populate('creator')
  if (!presentation) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Presentation with that id does not exist.' })
  }

  return res.status(200).json({ message: 'Presentation found.', data: presentation })
}

const list = async (req, res) => {
  const sort = req.query.sort
  const sortDirection = req.query.dir === 'asc' ? 1 : -1
  let presentations = null
  try {
    if (req.query.creator) {
      presentations = await PresentationModel
        .find({ creator: req.query.creator })
        .sort({ [sort]: sortDirection })
        .populate('creator')
    } else if (req.query.section) {
      presentations = await PresentationModel
        .find({ section: req.query.section })
        .sort({ [sort]: sortDirection })
        .populate('creator')
    } else {
      presentations = await PresentationModel
        .find({})
        .sort({ [sort]: sortDirection })
        .populate('creator')
    }
    const user = await UserModel.findById(req.user._id)
    const preses = presentations.map(pres => {
      const prog = user.progress.presentations.find(p => pres._id.equals(p.id))
      return {
        ...pres.toObject(),
        done: prog ? prog.done : false
      }
    })
    return res.status(200).json({ message: 'Presentations found.', data: preses })
  } catch (error) {
    logger.error(error)
    return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Server error' })
  }
}

const makeProgress = async (req, res) => {
  const presentation = await PresentationModel.findById(req.params.id)
  if (!presentation) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Presentation with that id does not exist.' })
  }
  if (!req.user.progress.presentations) {
    req.user.progress.presentations = []
  }
  let presentationProgress = req.user.progress.presentations.find(p => presentation._id.equals(p.id))
  if (presentationProgress) {
    presentationProgress.page = req.body.page + 1
    req.user.progress.money += money.PRESENTATION_PAGE_NEXT
  } else {
    presentationProgress = {
      id: presentation._id,
      pageCount: presentation.slides.length,
      page: req.body.page
    }
    req.user.progress.presentations.push(presentationProgress)
  }
  if (presentationProgress.page === presentationProgress.pageCount) {
    presentationProgress.done = true
    req.user.progress.money += money.PRESENTATION_PAGE_LAST
  }
  req.user.markModified('progress')

  try {
    await req.user.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }

  return res.status(202).json({ message: 'Progress made.', data: presentationProgress })
}

const rate = async (req, res) => {
  try {
    const presentation = await PresentationModel.findById(req.params.id).populate('creator')
    const rat = presentation.ratings.findIndex(p => req.user._id.equals(p.user))
    if (rat !== -1) {
      presentation.ratings.splice(rat, 1)
    }
    presentation.ratings.push({
      positive: req.body.positive,
      user: req.user._id
    })
    await presentation.save()
    return res.status(202).json({ message: 'Presentation rated.', data: presentation })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const edit = async (req, res) => {
  try {
    const presentation = await PresentationModel.findById(req.params.id)
    if (req.body.name) {
      presentation.name = req.body.name
    }
    if (req.body.slides) {
      presentation.slides = req.body.slides
    }
    if (req.body.section) {
      presentation.section = req.body.section
    }
    await presentation.save()
    return res.status(202).json({ message: 'Presentation updated.', data: presentation })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const remove = async (req, res) => {
  try {
    const presentation = await PresentationModel.findByIdAndDelete(req.params.id)
    return res.status(202).json({ message: 'Presentation deleted.', data: presentation })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

module.exports = {
  create,
  findOne,
  list,
  makeProgress,
  rate,
  edit,
  remove
}
