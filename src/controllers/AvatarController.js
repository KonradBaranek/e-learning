const { AvatarModel } = require('../models/AvatarModel')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')

const wear = async (req, res) => {
  const avatar = await AvatarModel.findOne({ user: req.user._id }).populate('ownedOutfits').populate('wornOutfits')
  if (!avatar) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Avatar with that id does not exist.' })
  }
  const outfit = avatar.ownedOutfits.find(outfit => outfit._id.equals(req.params.id))
  if (!outfit) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Outfit with that id does not exist.' })
  }
  const wornId = avatar.wornOutfits.findIndex(out => out.type === outfit.type)
  if (wornId >= 0) {
    avatar.wornOutfits[wornId] = outfit
  } else {
    avatar.wornOutfits.push(outfit)
  }
  avatar.markModified('wornOutfits')
  try {
    await avatar.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'Outfit bought.', data: avatar.wornOutfits })
}

const findForUser = async (req, res) => {
  const avatar = await AvatarModel.findOne({ user: req.user._id }).populate('ownedOutfits').populate('wornOutfits')
  if (!avatar) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Avatar with that id does not exist.' })
  }

  return res.status(200).json({ message: 'Avatar found.', data: avatar })
}

module.exports = {
  wear,
  findForUser
}
