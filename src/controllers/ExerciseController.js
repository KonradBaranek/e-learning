const { ExerciseModel } = require('../models/ExerciseModel')
const { UserModel } = require('../models/UserModel')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')
const Evaluator = require('../utils/evaluator')
const { shuffle } = require('../utils/random')
const money = require('../utils/money')

const start = async (req, res) => {
  const { user } = req
  const exercise = await ExerciseModel.findById(req.params.id)
  if (!exercise) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Exercise with that id does not exist.' })
  }
  if (!user.progress.exercises) {
    user.progress.exercises = []
  }

  const exerciseProgress = user.progress.exercises.find(e => e._id.equals(exercise._id))

  if (exerciseProgress) {
    return res.status(202).json({
      message: 'Exercise found.',
      data: exerciseProgress
    })
  }

  try {
    const variables = exercise.evaluate()
    const description = variables.reduce((description, { name, value }) => {
      const reg = new RegExp(`\\${name}`, 'g')
      return description.replace(reg, value)
    }, exercise.description)
    const answers = shuffle(
      exercise.answers.map(answer => {
        return {
          description: variables.reduce((description, { name, value }) => {
            const reg = new RegExp(`\\${name}`, 'g')
            return description.replace(reg, value)
          }, answer.description),
          correct: answer.correct
        }
      })
    )

    const newExerciseProgress = {
      _id: exercise._id,
      name: exercise.name,
      description,
      variables,
      answers
    }
    user.progress.exercises.push(newExerciseProgress)
    user.markModified('progress')
    await user.save()
    return res.status(202).json({
      message: 'Exercise found.',
      data: {
        name: newExerciseProgress.name,
        description: newExerciseProgress.description,
        answers: exercise.correct ? newExerciseProgress.answers : newExerciseProgress.answers.map(a => ({ description: a.description }))
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const answer = async (req, res) => {
  const { user } = req
  const { answer } = req.body
  const exercise = await ExerciseModel.findById(req.params.id)
  if (!exercise) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Exercise with that id does not exist.' })
  }
  if (!user.progress.exercises) {
    user.progress.exercises = []
  }
  const exerciseProgress = user.progress.exercises.find(e => e._id.equals(exercise._id))
  if (!exerciseProgress) {
    return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
  }
  exerciseProgress.answer = answer
  const isCorrect = exerciseProgress.answers.some(ans => ans === answer)
  exerciseProgress.isCorrect = isCorrect
  exerciseProgress.correct = exerciseProgress.answers.find(ans => ans.correct)
  if (isCorrect) {
    user.progress.money += money.EXERCISE_CORRECT_ANSWER
  } else {
    user.progress.money += money.EXERCISE_WRONG_ANSWER
  }
  exerciseProgress.done = true
  user.markModified('progress')
  try {
    await user.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }

  return res.status(202).json({
    message: 'Exercise answered.',
    data: exerciseProgress
  })
}

const check = async (req, res) => {
  const exercise = new ExerciseModel({
    name: req.body.name,
    description: req.body.description,
    variables: req.body.variables,
    answers: req.body.answers,
    section: req.body.section,
    creator: req.user._id
  })

  try {
    const variables = exercise.evaluate()
    const description = variables.reduce((description, { name, value }) => {
      const reg = new RegExp(`\\${name}`, 'g')
      return description.replace(reg, value)
    }, exercise.description)
    const answers = exercise.answers.map(answer => {
      return {
        description: variables.reduce((description, { name, value }) => {
          const reg = new RegExp(`\\${name}`, 'g')
          return description.replace(reg, value)
        }, answer.description),
        correct: answer.correct
      }
    })

    const newExerciseProgress = {
      _id: exercise._id,
      name: exercise.name,
      description,
      variables,
      answers
    }
    return res.status(201).json({ message: 'Exercise is correct.', data: newExerciseProgress })
  } catch (e) {
    return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: 'Exercise is not correct.' })
  }
}

const create = async (req, res) => {
  const isSomeCorrect = req.body.answers.some(a => a.correct)
  if (!isSomeCorrect) {
    return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: 'Some answer must be correct.' })
  }
  const everyAnswerHasDescription = req.body.answers.every(a => a.description)
  if (!everyAnswerHasDescription) {
    return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: 'Every answer must have a description.' })
  }
  const evaluator = new Evaluator(req.body.variables)
  try {
    evaluator.execute()
  } catch (error) {
    logger.error({
      message: 'Expression execution error',
      error
    })
    return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: 'Formula execution error.' })
  }
  const exercise = new ExerciseModel({
    name: req.body.name,
    description: req.body.description,
    variables: req.body.variables,
    answers: req.body.answers,
    section: req.body.section,
    creator: req.user._id
  })
  try {
    await exercise.save()
    req.user.progress.money += money.EXERCISE_CREATE
    req.user.markModified('progress')
    await req.user.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'Exercise created.', data: exercise })
}

const findOne = async (req, res) => {
  const exercise = await ExerciseModel.findById(req.params.id)
  if (!exercise) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Exercise with that id does not exist.' })
  }
  return res.status(200).json({ message: 'Exercise found.', data: exercise })
}

const list = async (req, res) => {
  const sort = req.query.sort
  const sortDirection = req.query.dir === 'asc' ? 1 : -1
  let exercises = null
  try {
    if (req.query.creator) {
      exercises = await ExerciseModel
        .find({ creator: req.query.creator })
        .sort({ [sort]: sortDirection })
        .populate('creator')
    } else if (req.query.section) {
      exercises = await ExerciseModel
        .find({ section: req.query.section })
        .sort({ [sort]: sortDirection })
        .populate('creator')
    } else {
      exercises = await ExerciseModel
        .find({})
        .sort({ [sort]: sortDirection })
        .populate('creator')
    }
    const user = await UserModel.findById(req.user._id)
    const exes = exercises.map(pres => {
      const prog = user.progress.exercises.find(p => pres._id.equals(p._id))
      return {
        ...pres.toObject(),
        done: prog ? (prog.correct || prog.done) : false
      }
    })
    return res.status(200).json({ message: 'Exercises found.', data: exes })
  } catch (error) {
    logger.error(error)
    return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Server error' })
  }
}

const rate = async (req, res) => {
  try {
    const presentation = await ExerciseModel.findById(req.params.id).populate('creator')
    const rat = presentation.ratings.findIndex(p => req.user._id.equals(p.user))
    if (rat !== -1) {
      presentation.ratings.splice(rat, 1)
    }
    presentation.ratings.push({
      positive: req.body.positive,
      user: req.user._id
    })
    await presentation.save()
    req.user.progress.money += money.EXERCISE_RATE
    req.user.markModified('progress')
    await req.user.save()
    return res.status(202).json({ message: 'Presentation rated.', data: presentation })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const edit = async (req, res) => {
  try {
    const exercise = await ExerciseModel.findById(req.params.id)
    if (req.body.name) {
      exercise.name = req.body.name
    }
    if (req.body.variables) {
      exercise.variables = req.body.variables
    }
    if (req.body.section) {
      exercise.section = req.body.section
    }
    if (req.body.description) {
      exercise.description = req.body.description
    }
    if (req.body.answers) {
      exercise.answers = req.body.answers
    }
    await exercise.save()
    return res.status(202).json({ message: 'Exercise updated.', data: exercise })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const remove = async (req, res) => {
  try {
    const exercise = await ExerciseModel.findByIdAndDelete(req.params.id)
    return res.status(202).json({ message: 'Exercise deleted.', data: exercise })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

module.exports = {
  start,
  answer,
  create,
  check,
  findOne,
  rate,
  list,
  edit,
  remove
}
