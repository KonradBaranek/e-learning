const { UserModel } = require('../models/UserModel')
const { AvatarModel } = require('../models/AvatarModel')
const svgUtils = require('../utils/svgUtils')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')

const login = async (req, res) => {
  const user = await UserModel.findOne({ email: req.body.email })
  if (!user) {
    return res.status(422).json({ errorCode: codes.DOES_NOT_EXIST, message: 'User with this email address does not exist.' })
  }
  const isMatch = await user.comparePassword(req.body.password)
  if (!isMatch) {
    return res.status(422).json({ errorCode: codes.WRONG_CREDENTIALS, message: 'Wrong password.' })
  }
  user.lastLogin = Date.now()
  await user.save()
  return res.status(202).json({ message: 'User logged in.', token: user.generateToken(), user: user.stripSensitive() })
}

const register = async (req, res) => {
  if (await UserModel.findOne({ email: req.body.email })) {
    return res.status(422).json({ errorCode: codes.ALREADY_EXISTS, message: 'User with this email already exists.' })
  }
  const user = new UserModel({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    role: 'user',
    progress: {
      money: 100,
      exercises: [],
      presentations: []
    }
  })
  try {
    await user.save()

    AvatarModel.create({
      name: user.username || 'Test',
      ownedOutfits: [],
      wornOutfits: [],
      user: user._id,
      svg: svgUtils.avatar
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'User saved.' })
}

const edit = async (req, res) => {
  const { user } = req
  const newUser = {
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  }

  try {
    const same = await UserModel.find({ email: newUser.email })
    if (same.length) {
      return res.status(402).json({ errorCode: codes.ALREADY_EXISTS, message: 'Email already exists.' })
    }
    const updated = await UserModel.findOneAndUpdate({ _id: user._id }, newUser, { new: true })
    return res.status(201).json({ message: 'User saved.', data: updated.toObject() })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
}

const me = async (req, res) => {
  const user = UserModel.findById(req.user._id)
  return res.status(200).json({ message: 'Me', data: user })
}

module.exports = {
  edit,
  login,
  register,
  me
}
