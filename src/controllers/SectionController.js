const { SectionModel } = require('../models/SectionModel')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')

const create = async (req, res) => {
  const section = new SectionModel({
    name: req.body.name,
    description: req.body.description
  })
  try {
    await section.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'Section created.', data: section })
}

const findOne = async (req, res) => {
  const section = await SectionModel.findById(req.params.id)
  if (!section) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Section with that id does not exist.' })
  }

  return res.status(200).json({ message: 'Section found.', data: section })
}

const list = async (req, res) => {
  const sort = req.query.sort
  const sortDirection = req.query.dir === 'asc' ? 1 : -1
  let sections
  if (req.query.grade) {
    sections = await SectionModel
      .find({ grade: req.query.grade })
      .sort({ [sort]: sortDirection })
  } else {
    sections = await SectionModel
      .find({})
      .sort({ [sort]: sortDirection })
  }

  return res.status(200).json({ message: 'Sections found.', data: sections })
}

module.exports = {
  create,
  findOne,
  list
}
