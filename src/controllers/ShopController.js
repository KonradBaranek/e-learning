const { AvatarModel } = require('../models/AvatarModel')
const { OutfitModel } = require('../models/OutfitModel')
const { UserModel } = require('../models/UserModel')
const codes = require('../utils/errorCodes')
const logger = require('../utils/logger')

const buy = async (req, res) => {
  const outfit = await OutfitModel.findById(req.params.id);
  const user = await UserModel.findById(req.user._id);
  if (!outfit) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Outfit with that id does not exist.' })
  }
  const { money } = user.progress
  if (money < outfit.price) {
    return res.status(400).json({ errorCode: codes.VALIDATION_ERROR, message: 'Not enough money.' })
  }

  user.progress.money = user.progress.money - outfit.price;

  try {
    await AvatarModel.update(
      { user: req.user._id },
      { $push: { ownedOutfits: outfit } }
    )
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  try {
    user.markModified('progress.money');
    await user.save()
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(422).json({ errorCode: codes.VALIDATION_ERROR, message: Object.values(error.errors).map(e => e.message).join(' ') })
    } else {
      logger.error(error)
      return res.status(500).json({ errorCode: codes.INTERNAL_SERVER_ERROR, message: 'Internal server error.' })
    }
  }
  return res.status(201).json({ message: 'Outfit bought.', data: {outfit, money: user.progress.money } })
}

const findOne = async (req, res) => {
  const outfit = await OutfitModel.findById(req.params.id)
  if (!outfit) {
    return res.status(404).json({ errorCode: codes.DOES_NOT_EXIST, message: 'Outfit with that id does not exist.' })
  }

  return res.status(200).json({ message: 'Outfit found.', data: outfit })
}

const list = async (req, res) => {
  const sort = req.query.sort
  const sortDirection = req.query.dir === 'asc' ? 1 : -1
  const outfits = await OutfitModel
    .find({})
    .sort({ [sort]: sortDirection })

  return res.status(200).json({ message: 'Outfits found.', data: outfits })
}

module.exports = {
  buy,
  findOne,
  list
}
