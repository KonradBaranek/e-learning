const passport = require('passport')
const { Strategy: JWTStrategy, ExtractJwt: ExtractJWT } = require('passport-jwt')
const { UserModel } = require('../src/models/UserModel')

passport.serializeUser((user, done) => {
  done(undefined, user.id)
})

passport.deserializeUser((id, done) => {
  UserModel.findById(id, (err, user) => {
    done(err, user)
  })
})

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET
}, (payload, done) => {
  UserModel.findById(payload._id)
    .then(user => {
      if (user) {
        return done(null, user)
      }
      return done(null, false)
    })
    .catch(error => {
      return done(error, false)
    })
}))
