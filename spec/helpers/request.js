const axios = require('axios')
const request = (path, method = 'get', data = null) => {
  return axios({
    url: `http://localhost:${process.env.PORT}/${path}`,
    method,
    data
  })
}
module.exports = {
  request
}
