require('dotenv').config()
const { request } = require('../helpers/request')
const server = require('../../src/server')
const { UserModel } = require('../../src/models/UserModel')

describe('User Controller', () => {
  const validUser = {
    email: 'name@mail.com',
    password: 'VeryLongPassword1$',
    role: 'user'
  }
  const invalidUser = {
    invalidEmail: 'test.mail.com',
    invalidPassword: 'VeryLongPassword1$',
    invalidRole: 'user',
    email: 'name@mail.com',
    password: 'VeryLongPassword1$',
    role: 'user'
  }
  let serverInstance = null
  beforeAll(done => {
    serverInstance = server.listen(process.env.PORT, () => {
      UserModel.findOneAndDelete({ email: validUser.email }, () => {
        done()
      })
    })
  })
  describe('register', () => {
    it('registers new user', done => {
      request('api/1/user/register/', 'post', {
        email: validUser.email,
        password: validUser.password,
        role: validUser.role
      })
        .then(response => {
          expect(response.status).toBe(201)
          done()
        })
        .catch(error => {
          console.error(error)
          expect(error).toBeUndefined()
          done()
        })
    })
    it('fails to register invalid user email', done => {
      request('api/1/user/register/', 'post', {
        email: invalidUser.invalidEmail,
        password: invalidUser.password,
        role: invalidUser.role
      })
        .then(response => {
          expect(response.status).not.toBe(201)
          done()
        })
        .catch(error => {
          expect(error).not.toBeUndefined()
          done()
        })
    })
    it('fails to register invalid user role', done => {
      request('api/1/user/register/', 'post', {
        email: invalidUser.email,
        password: invalidUser.password,
        role: invalidUser.invalidRole
      })
        .then(response => {
          expect(response.status).not.toBe(201)
          done()
        })
        .catch(error => {
          expect(error).not.toBeUndefined()
          done()
        })
    })
    it('fails to register invalid user password', done => {
      request('api/1/user/register/', 'post', {
        email: invalidUser.email,
        password: invalidUser.invalidPassword,
        role: invalidUser.role
      })
        .then(response => {
          expect(response.status).not.toBe(201)
          done()
        })
        .catch(error => {
          expect(error).not.toBeUndefined()
          done()
        })
    })
  })
  describe('login', () => {
    it('logs in registered user', done => {
      request('api/1/user/login/', 'post', {
        email: validUser.email,
        password: validUser.password
      })
        .then(response => {
          expect(response.status).toBe(202)
          done()
        })
        .catch(error => {
          console.error(error.response.data)
          expect(error).toBeUndefined()
          done()
        })
    })
    it('fails to logs in not existing user', done => {
      request('api/1/user/login/', 'post', {
        email: invalidUser.invalidEmail,
        password: invalidUser.invalidPassword
      })
        .then(response => {
          expect(response.status).not.toBe(202)
          done()
        })
        .catch(error => {
          expect(error).not.toBeUndefined()
          done()
        })
    })
  })
  afterAll(done => {
    UserModel.findOneAndDelete({ email: validUser.email }, () => {
      serverInstance.close()
      done()
    })
  })
})
